---
date: '2016-04-01'
title: 'Slip-n-Solve'
github: 'https://github.com/kajallochab/Slip-n-Solve'
external: ''
tech:
  - JavaScript
  - CSS
  - HTML
company: ''
showInProjects: true
---

A sliding puzzle game made with HTML, CSS and JS