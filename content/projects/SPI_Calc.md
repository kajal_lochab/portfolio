---
date: '2019-11-12'
title: 'ITNU-CSE-SPI-Calculator'
github: ''
external: 'https://github.com/kajallochab/ITNU-CSE-SPI-Calculator'
tech:
  - JavaScript
  - CSS
  - HTML
company: ''
showInProjects: true
---

This project is a simple tool designed to help students at the Institute of Technology, Nirma University, calculate their Semester Performance Index (SPI) effortlessly.
