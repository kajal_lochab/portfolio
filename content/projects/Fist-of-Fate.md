---
date: '2017-12-01'
title: 'Fist-of-Fate'
github: ''
external: 'https://github.com/kajallochab/Fist-of-Fate'
tech:
  - JavaScript
  - CSS
  - HTML
company: ''
showInProjects: true
---

A simple Rock Paper Scissor game with HTML, CSS and JS.
