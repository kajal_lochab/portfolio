---
date: '2017-08-01'
title: 'Portfolio 1'
github: ''
external: 'https://delightful-mud-07d6a8600.4.azurestaticapps.net'
tech:
  - JavaScript
  - CSS
  - HTML
  - Azure
company: 'Scout'
showInProjects: false
---

JavaScript Portfolio Site with GitHub Codespaces and Copilot as a part of MLSA.
