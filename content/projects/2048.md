---
date: '2020-03-27'
title: '2048-PythonPink'
github: 'https://github.com/kajallochab/2048-PythonPink'
external: ''
tech:
  - Python
  - Tkinter
company: ''
showInProjects: true
---

"2048" is a Python-based puzzle game where the player merges numbered tiles on a 4x4 grid to reach the tile with the number 2048, using strategic thinking and avoiding a filled grid. 