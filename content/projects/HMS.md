---
date: '2017-11-01'
title: 'Hostel-Management-System'
github: ''
external: 'https://github.com/kajallochab/Hostel-Management-System'
tech:
  - PHP
  - CSS
company: ''
showInProjects: true
---

A straightforward hostel management system featuring a few functional modules that can be easily expanded as required. The code is intentionally basic, designed to serve as a platform for exploration and experimentation with PHP while implementing fundamental database management system (DBMS) concepts.
