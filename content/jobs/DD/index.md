---
date: '2017-04-01'
title: 'Writing Intern'
company: 'DefEating Disorders'
location: 'Gurugram, Haryana'
range: 'June 2021 - April 2022'
url: 'https://defeatingdisorders.com/'
---

- Content Writer: Wrote articles and poems on various eating disorders like pica, atypical anorexia, bulimia, etc.
- Script Writer: Contributed to the scripts for interview with Ms. Amita Gondi and Ms. Arusha Jha.