---
date: '2022-08-11'
title: 'Women Engineering Program'
company: 'TalentSprint and supported by Google'
location: 'Remote'
range: 'March 2022 - March 2024'
url: 'https://we.talentsprint.com/'
---

-  Selected as one of the 250 out of 30,000+ applicants to TalentSprint’s Women Engineers Programme supported by Google.
-  During this program, I developed my technical skills in Python and learned essential corporate skills such as time
management, goal setting, project management, and design thinking.